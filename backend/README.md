Jobs Backend
=============================
Backend Jobs is simple jobs application written in Go.

## Configuration
Jobs Backend read configuration from environment variable

| ENV        |      Description                 |
|------------|:---------------------------------|
| APP_PORT   | run jobsbackend in specific port |
| MONGO_HOST | mongodb hostname                 |
| MONGO_USER | mongodb user                     |
| MONGO_PASS | mongodb password                 |
| MONGO_PORT | mongodb port                     |



  
## Endpoint
- /jobs
  - `GET` get list job
- /job
  - `POST` create a job  
- /job/id
  - `GET` get job
  - `DELETE` delete job
- /health
  - `GET` check app health status


NOTED: This Backend from KarsaJobs Dicoding [click here](https://github.com/dicodingacademy/karsajobs)